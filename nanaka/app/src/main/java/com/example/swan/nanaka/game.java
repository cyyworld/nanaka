package com.example.swan.nanaka;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class game extends AppCompatActivity {


    ValueAnimator arrowAnimator, cloudAnimatior, throwBallAnimator, fallBallAnimator, viewAnimator, birdsAnimator;

    ImageView cloud1, ball,arrow, birds;
    TextView view_count, view_point, view_distance;

    RelativeLayout gameView;
    Context context;
    float DISTACNE = 0.f;
    int device_width, device_height;
    int BOOSTER_GAGE = 0;
    int VIEW_CLICK_COUNT = 5;
    int CLOUD_DURATION = 2000;

    boolean BOOSTER_USED = false;
    boolean BALL_STOP = false;
    boolean SHOOT_BALL = false;
    Toast POINT_TOAST;
    String LOG_TAG = "NANAKA";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics dm = getApplicationContext().getResources().getDisplayMetrics();
        device_width =  dm.widthPixels;
        device_height = dm.heightPixels;
        context = this;

        POINT_TOAST = Toast.makeText(context, "충돌!", Toast.LENGTH_LONG);
        Log.d("aaaaa","device_width : "+ device_width);
        Log.d("aaaaa","device_height : "+ device_height);

        setContentView(R.layout.activity_game);
        ball = new ImageView(context);
        birds = new ImageView(context);
        gameView = findViewById(R.id.gameView);

        cloud1 = new ImageView(context);
        arrow = new ImageView(context);

        drawBackground();
    }
    void drawBackground(){

        view_point = new TextView(context);
        view_point.setText("Booster !!!");
        view_point.setTextSize(50.f);
        view_point.setTextColor(Color.WHITE);

        view_distance = new TextView(context);
        view_distance.setText(DISTACNE + "m");
        view_distance.setTextSize(30.f);
        view_distance.setTextColor(Color.WHITE);
        view_distance.setTranslationX(device_width*0.7f);
        view_distance.setTranslationY(device_height*0.05f);

        view_count = new TextView(context);
        view_count.setText("BOOSTER : " + VIEW_CLICK_COUNT);
        view_count.setTextSize(30.f);
        view_count.setTextColor(Color.WHITE);
        view_count.setTranslationX(device_width*0.02f);
        view_count.setTranslationY(device_height*0.05f);


        ball.setImageResource(R.drawable.circle);
        cloud1.setImageResource(R.drawable.gemicon);
        float cloud_width = device_width*0.11f;
        float cloud_height = cloud_width;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) cloud_width, (int)cloud_height);
        layoutParams.setMargins(0,0,0,0);
        cloud1.setLayoutParams(layoutParams);

        view_point.setVisibility(View.INVISIBLE);
        cloud1.setTranslationX(device_width);
        cloud1.setTranslationY(device_height*0.95f - cloud_height);

        birds.setImageResource(R.drawable.birds);
        int birds_width = (int)(device_width*0.2);
        int birds_height = (int)(birds_width*0.44);
        RelativeLayout.LayoutParams birds_layoutParams = new RelativeLayout.LayoutParams(birds_width, birds_height);
        birds.setLayoutParams(birds_layoutParams);
        birds.setTranslationX(device_width);
        birds.setTranslationY(device_height*0.1f);


        arrow.setImageResource(R.drawable.arrow);
        int arrow_width = (int)(device_width*0.08);
        RelativeLayout.LayoutParams arrow_layoutParams = new RelativeLayout.LayoutParams(arrow_width, arrow_width);
        arrow.setLayoutParams(arrow_layoutParams);
        arrow.setPivotX(0);
        arrow.setPivotY(arrow_width);
        arrow.setTranslationX(device_width*0.15f);
        arrow.setTranslationY(device_height*0.6f);


        RelativeLayout.LayoutParams ball_layoutParams = new RelativeLayout.LayoutParams(device_height/6, device_height/6);
        ball.setLayoutParams(ball_layoutParams);
        ball.setTranslationX(device_width*0.15f - device_height/12);
        ball.setTranslationY(device_height*0.6f + device_width*0.08f - device_height/12);

        gameView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setOnClickListener(null);
                SHOOT_BALL = true;
                throwBall(ball.getTranslationX(), ball.getTranslationY(), 45.f-arrow.getRotation());
                arrow.setVisibility(View.INVISIBLE);

            }
        });

        gameView.addView(view_count);
        gameView.addView(view_point);
        gameView.addView(cloud1);
        gameView.addView(ball);
        gameView.addView(arrow);
        gameView.addView(view_distance);
        gameView.addView(birds);

        ball.bringToFront();
        view_point.bringToFront();
        birdsAnimation();
        arrowAnimation(false,1500);
    }
    void arrowAnimation(final boolean is_reverse, final int dur){
        if(SHOOT_BALL)return;
        if(!is_reverse){
            arrowAnimator = ValueAnimator.ofFloat(-40,40); // values from 0 to 1
        }else{
            arrowAnimator = ValueAnimator.ofFloat(40,-40); // values from 0 to 1
        }
        arrowAnimator.setDuration(dur);
        arrowAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
        {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = ((Float) (animation.getAnimatedValue())).floatValue();
                arrow.setRotation(value);
            }
        });

        arrowAnimator.addListener(new AnimatorListenerAdapter()
        {
            @Override
            public void onAnimationEnd(Animator animation)
            {
                arrowAnimator.removeAllUpdateListeners();
                arrowAnimator.removeAllListeners();
                arrowAnimator = null;
                arrowAnimation(!is_reverse,dur);
            }
        });
        arrowAnimator.setInterpolator(AnimationUtils.loadInterpolator(this, android.R.anim.linear_interpolator));
        arrowAnimator.start();
    }


    void birdsAnimation(){
        birdsAnimator = ValueAnimator.ofFloat(device_width,(-device_width*0.2f)); // values from 0 to 1
        birdsAnimator.setDuration(3000);
        birdsAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(){
            public  int count = 0;
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = ((Float) (animation.getAnimatedValue())).floatValue();
                if(BOOSTER_GAGE > 50){

                    int inc = (int)(BOOSTER_GAGE*0.02);
                    count+=inc;
                    BOOSTER_GAGE -= inc;
                }
                birds.setTranslationX(value-count);
            }
        });

        birdsAnimator.addListener(new AnimatorListenerAdapter(){
            @Override
            public void onAnimationEnd(Animator animation)
            {
                birdsAnimator.removeAllUpdateListeners();
                birdsAnimator.removeAllListeners();
                birdsAnimator = null;
                birdsAnimation();
            }
        });
        birdsAnimator.setInterpolator(AnimationUtils.loadInterpolator(this, android.R.anim.accelerate_interpolator));
        birdsAnimator.start();
    }
    void cloudAnimatioin(){
        if(BALL_STOP)return;
        double random = Math.random();
        int dur = CLOUD_DURATION;
        if(random < 0.3){
            cloud1.setVisibility(View.INVISIBLE);
            dur = (int)(Math.random()*4000+1000);
        }else{
            cloud1.setVisibility(View.VISIBLE);
        }

        cloudAnimatior = ValueAnimator.ofFloat(device_width,(-device_width*0.2f)); // values from 0 to 1
        cloudAnimatior.setDuration(dur);
        cloudAnimatior.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(){
            public  int count = 0;
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                if(BALL_STOP) return;
                float value = ((Float) (animation.getAnimatedValue())).floatValue();
                if(BOOSTER_GAGE > 50){

                    int inc = (int)(BOOSTER_GAGE*0.02);
                    count+=inc;
                    BOOSTER_GAGE -= inc;
                    DISTACNE += inc/5;
                }else{
                    BOOSTER_GAGE = 0;
                }
                if((value-count) < (-device_width*0.2f)){
                    cloudAnimatior.removeAllUpdateListeners();
                    cloudAnimatior.removeAllListeners();
                    cloudAnimatior = null;
                    cloudAnimatioin();
                }else{
                    cloud1.setTranslationX(value-count);
                }

            }
        });

        cloudAnimatior.addListener(new AnimatorListenerAdapter(){
            @Override
            public void onAnimationEnd(Animator animation)
            {
                cloudAnimatior.removeAllUpdateListeners();
                cloudAnimatior.removeAllListeners();
                cloudAnimatior = null;
                cloudAnimatioin();
            }
        });
        cloudAnimatior.setInterpolator(AnimationUtils.loadInterpolator(this, android.R.anim.linear_interpolator));
        cloudAnimatior.start();
    }
    void throwBall(final float x, final float y, final float degree){
        gameView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(VIEW_CLICK_COUNT>0){
                   BOOSTER_USED = true;
                   BOOSTER_GAGE += 3000;
                   VIEW_CLICK_COUNT --;
                   view_count.setText("BOOSTER : " + VIEW_CLICK_COUNT);
                   showPoint();
               }

            }
        });

        throwBallAnimator= ValueAnimator.ofFloat(x, device_width*0.4f); // values from 0 to 1
        throwBallAnimator.setDuration(1000);
        throwBallAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
        {
            public float goal_height;
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = ((Float) (animation.getAnimatedValue())).floatValue();
                float value_y = y-(value-x)*((float)Math.tan((float)Math.toRadians(degree)));
                float scaleValue;
                DISTACNE += value/300;
                view_distance.setText(String.format("%.2f", DISTACNE) + "m");
                if(value_y < 10){
                    return;
                }else{
                    ball.setTranslationY(value_y);
                    ball.setTranslationX(value);
                }
            }
        });
        throwBallAnimator.addListener(new AnimatorListenerAdapter()
        {
            @Override
            public void onAnimationEnd(Animator animation)
            {
                throwBallAnimator.removeAllUpdateListeners();
                throwBallAnimator.removeAllListeners();
                throwBallAnimator = null;

                int dur = (int)(((device_height - ball.getTranslationY())*1000)/device_height);

                fallBall(dur, false, ball.getTranslationY());
                cloudAnimatioin();
            }
        });
        throwBallAnimator.setInterpolator(AnimationUtils.loadInterpolator(this, android.R.anim.linear_interpolator));
        throwBallAnimator.start();
    }

    void fallBall(final int fallingSpeed,  final boolean isUp, final float height){
        Log.d(LOG_TAG,"spd :  "+ fallingSpeed);
         float ball_y = ball.getTranslationY();
         if(!isUp && height > (device_height - device_height/4.)){
            BALL_STOP = true;
            return;
        }
        if(isUp){
            fallBallAnimator  = ValueAnimator.ofFloat((float)(Math.sqrt(Math.abs(ball_y))), (float)(Math.sqrt(Math.abs(height))));
        }else{
            fallBallAnimator  = ValueAnimator.ofFloat((float)(Math.sqrt(Math.abs(ball_y))), (float)Math.sqrt(device_height - device_height/5));
        }
        fallBallAnimator.setDuration(fallingSpeed);
        fallBallAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
        {
            public boolean CHECK_COLLISION = false;
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                    float value = ((Float) (animation.getAnimatedValue())).floatValue();
                    ball.setTranslationY((float)(value*value));
                    DISTACNE += value/200;
                    if(cloud1.getVisibility() == View.VISIBLE && !CHECK_COLLISION &&
                            ball.getTranslationX() >= (cloud1.getTranslationX()-device_height/6) &&
                            ball.getTranslationX() <= (cloud1.getTranslationX()) + device_width*0.11f &&
                            ball.getTranslationY()+10 >= cloud1.getTranslationY()
                            ) {
                        CHECK_COLLISION = true;
                        BOOSTER_GAGE += 3000;
                        cloud1.setVisibility(View.INVISIBLE);
                        showPoint();
                    }
                view_distance.setText(String.format("%.2f", DISTACNE) + "m");
            }
        });

        fallBallAnimator.addListener(new AnimatorListenerAdapter()
        {
            public boolean BOOSTER_USED = false;
            public int beforeBoosterGage = 0;
            @Override
            public void onAnimationStart(Animator animation) {
                if(BOOSTER_GAGE > 0) BOOSTER_USED = true;
            }
            @Override
            public void onAnimationEnd(Animator animation)
            {
                boolean isUpBooster = false;
                if(BOOSTER_USED && isUp){
                    isUpBooster = true;
                }
                BOOSTER_USED =false;

                fallBallAnimator.removeAllUpdateListeners();
                fallBallAnimator.removeAllListeners();
                fallBallAnimator = null;

                float ht, spdValue = 1000;
                int dur;
                ht = height;
                if(BOOSTER_USED || BOOSTER_GAGE > 0){
                    spdValue = 800;
                    ht -= (device_height-ht)*0.05;
                    if(ht<10) ht = 10;

                }else{
                    ht += (device_height-ht)*0.1;
                    CLOUD_DURATION += 200;
                }

                if(isUpBooster){
                    dur = (int)(((Math.abs(ball.getTranslationY() - ht))*500)/device_height);
                    fallBall(dur,true, ht);
                }else{
                    dur = (int)(((device_height - ht)*spdValue)/device_height);
                    fallBall(dur,!isUp, ht);
                }

            }
        });
        if(isUp){
            fallBallAnimator.setInterpolator(AnimationUtils.loadInterpolator(this, android.R.anim.decelerate_interpolator));
        }else{
            fallBallAnimator.setInterpolator(AnimationUtils.loadInterpolator(this, android.R.anim.accelerate_interpolator));
        }
        fallBallAnimator.start();
    }

    void showPoint(){

        if(viewAnimator!=null){
            viewAnimator.removeAllUpdateListeners();
            viewAnimator.removeAllListeners();
            viewAnimator = null;
        }

        view_point.setVisibility(View.VISIBLE);
        view_point.setTranslationX(device_width*0.4f);
        view_point.setTranslationX(device_height*0.7f);

        viewAnimator = ValueAnimator.ofFloat(device_height*0.5f, device_height*0.2f); // values from 0 to 1
        viewAnimator.setDuration(1000);
        viewAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
        {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = ((Float) (animation.getAnimatedValue())).floatValue();
                view_point.setTranslationY(value);
            }
        });
        viewAnimator.addListener(new AnimatorListenerAdapter()
        {
            @Override
            public void onAnimationEnd(Animator animation)
            {
                viewAnimator.removeAllUpdateListeners();
                viewAnimator.removeAllListeners();
                viewAnimator = null;
                view_point.setVisibility(View.INVISIBLE);
            }
        });
        viewAnimator.setInterpolator(AnimationUtils.loadInterpolator(this, android.R.anim.accelerate_interpolator));
        viewAnimator.start();
    }
    void pauseAnimation(){
        if(arrowAnimator != null && arrowAnimator.isRunning())arrowAnimator.pause();
        if(cloudAnimatior != null && cloudAnimatior.isRunning())cloudAnimatior.pause();
        if(throwBallAnimator != null && throwBallAnimator.isRunning())throwBallAnimator.pause();
        if(fallBallAnimator != null && fallBallAnimator.isRunning())fallBallAnimator.pause();
        if(viewAnimator != null && viewAnimator.isRunning())viewAnimator.pause();
        if(birdsAnimator != null && birdsAnimator.isRunning())birdsAnimator.pause();


    }
    void resumeAnimation(){
        if(arrowAnimator != null && arrowAnimator.isPaused())arrowAnimator.resume();
        if(cloudAnimatior != null && cloudAnimatior.isPaused())cloudAnimatior.resume();
        if(throwBallAnimator != null  && throwBallAnimator.isPaused())throwBallAnimator.resume();
        if(fallBallAnimator != null  && fallBallAnimator.isPaused())fallBallAnimator.resume();
        if(viewAnimator != null  && viewAnimator.isPaused())viewAnimator.resume();
        if(birdsAnimator != null  && birdsAnimator.isPaused())birdsAnimator.resume();

    }
    void destroyAnimation(){
        if(arrowAnimator != null){
            arrowAnimator.removeAllUpdateListeners();
            arrowAnimator.removeAllListeners();
            arrowAnimator = null;
        }
        if(cloudAnimatior != null){
            cloudAnimatior.removeAllUpdateListeners();
            cloudAnimatior.removeAllListeners();
            cloudAnimatior = null;
        }
        if(throwBallAnimator != null){
            throwBallAnimator.removeAllUpdateListeners();
            throwBallAnimator.removeAllListeners();
            throwBallAnimator = null;
        }
        if(fallBallAnimator != null){
            fallBallAnimator.removeAllUpdateListeners();
            fallBallAnimator.removeAllListeners();
            fallBallAnimator = null;
        }
        if(viewAnimator != null){
            viewAnimator.removeAllUpdateListeners();
            viewAnimator.removeAllListeners();
            viewAnimator = null;
        }
        if(birdsAnimator != null){
            birdsAnimator.removeAllUpdateListeners();
            birdsAnimator.removeAllListeners();
            birdsAnimator = null;
        }


    }
    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
        resumeAnimation();
    }
    @Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
    @Override
    protected void onPause() {
        super.onPause();
        // Another activity is taking focus (this activity is about to be "paused").
    }
    @Override
    protected void onStop() {
        super.onStop();
        pauseAnimation();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        destroyAnimation();
    }
}
